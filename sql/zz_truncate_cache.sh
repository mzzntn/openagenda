#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "openagenda_prototipo" <<-EOSQL
TRUNCATE ezdfsfile_cache;
EOSQL
