## Procedura manuale per creare una nuova istanza di openpa


 * Eseguire lo script di creazione dei settings (disponibile dalla versione 2.27 di openpa) **nella propria installazione locale**

```
php extension/openpa/bin/php/create_instance.php -sprototipo_backend
```

 * Commit del codice nel repo codecommit
 
 * Eseguire deploy dei settings nel **server cron**
```
ssh 10.240.0.125
cd /usr/share/nginx/saasopenpa-distribution/; git pull; composer install;
```  

 * Creazione del core solr e aggiornamento del repository nel **server solr**
```
ssh 10.240.0.68
sudo su -
./ez_solr_cores_per_nuovo_solr.sh -a IDENTIFICATIVO_NUOVA_ISTANZA
service solr-multicore-8999 restart
cd multicore-8999
git add --all
git commit -m "aggiunta istanza IDENTIFICATIVO_NUOVA_ISTANZA"
git push
``` 

 * Eseguire lo script di creazione dei database nel **server cron**
 
```
cd ~/scripts/openpa_aws_create/
./openpa_cluster_create.sh  --nome IDENTIFICATIVO_NUOVA_ISTANZA \
                              --nome-lungo "non utilizzato" \
                              --indirizzo "www.non-utilizzato.it" \
                              --db-server db.saasopenpa-astratto   \
                              --db-admin-user root \
                              --db-admin-password ****** \
                              --db-template ez20150103_openpa_prototipo
```

 * Eseguire gli script di clusterizzazione nel **server cron**

```
cd /usr/share/nginx/saasopenpa-distribution/html/
php bin/php/clusterize.php  --siteaccess=cterredadige_backend -n -q > /mnt/efs/cluster-openpa/bucket_s3/log_clusterize_istanze/clusterize_cterredadige.log
php extension/bfsurveyfile/bin/php/clusterize.php  --siteaccess=cterredadige_backend -q > /mnt/efs/cluster-openpa/bucket_s3/log_clusterize_istanze/clusterize_cterredadige_bfsurveyfile.log
php extension/ezflip/bin/php/clusterize.php        --siteaccess=cterredadige_backend -q > /mnt/efs/cluster-openpa/bucket_s3/log_clusterize_istanze/clusterize_cterredadige_ezflip.log
php openpa_tools/batch/clusterize_ezflowmedia.php --siteaccess=cterredadige_backend -q > /mnt/efs/cluster-openpa/bucket_s3/log_clusterize_istanze/clusterize_cterredadige_ezflowmedia.log
```

 * Eseguire gli script di inizializzazione nel **server cron**
 
```
cd /usr/share/nginx/saasopenpa-distribution/html/
php extension/openpa/bin/php/init.php -scterredadige_backend; php bin/php/ezcache.php --clear-all -scterredadige_backend;
```

 * Creare il virtual host di nginx **nella propria installazione locale** (lo script non è ancora disponibile)
 
 * Creare il dominio IDENTIFICATIVO_NUOVA_ISTANZA.saasopenpa.opencontent.it in **Route53** come alias di varnish
 
 * Eseguire deploy del virtual host nginx nel **server cron**
``` 
cd /etc/nginx/;sudo git pull origin master;sudo nginx -t;sudo service nginx reload
``` 

 * Eseguire il deploy del codice ez e dei virtual host nginx **in tutti gli nginx**
``` 
cd /usr/share/nginx/saasopenpa-distribution/; git pull; composer install;
cd /etc/nginx/;sudo git pull origin master;sudo nginx -t;sudo service nginx reload
``` 